package it.wlf;

import java.io.IOException;
import it.wlf.net.SocketListener;

public class Main {

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String debugMode = "";
        try {
            debugMode = args[4];
        } catch (Exception e) {
        }
        if (debugMode.toUpperCase().equals("CUSTOMRACE")) {
            System.out.println("Run custom race");
            host = "hakkinen.helloworldopen.com";
            Thread t = new SocketListener(host, port, botName, botKey, "MYPRIVATERACE", "germany", 4, true);
            t.start();
        } else {
            System.out.println("Start with:" + host + " " + port + " " + botName + " " + botKey);
            Thread listThread = new SocketListener(host, port, botName, botKey);
            listThread.start();
        }
    }
}
