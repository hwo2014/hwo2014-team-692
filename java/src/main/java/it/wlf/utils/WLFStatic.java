package it.wlf.utils;

import com.google.gson.Gson;
import it.wlf.core.calc.Physics;
import it.wlf.core.components.Car;
import it.wlf.core.components.Pilot;
import it.wlf.core.components.Track;
import it.wlf.core.components.TurboEngine;
import it.wlf.core.json.SendMsg;
import it.wlf.core.json.SendMsgAdvanced;
import it.wlf.telemetry.Telemetry;
import java.io.PrintWriter;

/**
 * Final and static
 *
 * @author Melknix
 */
public class WLFStatic {
    
//<editor-fold defaultstate="collapsed" desc="Constants">
    public static final double GRAVITY = 9.85;
    /**
     * Dummy multiplier for mass
     */
    public static final double MASSMULTIPLIER = 1;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Attributes">
    /**
     * GSON google json parser
     */
    public static final Gson gson = new Gson();
    /**
     * Store my car data
     */
    public static final Car myCar = new Car();
    /**
     * Tell if the race is a quick race or a tournament
     */
    public static boolean isQuickRace = true;
    /**
     * Track information
     */
    public static Track track = new Track();    
    /**
     * Physics information
     */
    public static Physics physics = new Physics();    
    /**
     * Pylot instance
     */
    public static final Pilot myPilot = new Pilot();
    /**
     * Turbo information
     */
    public static TurboEngine turboEngine = new TurboEngine();
    /**
     * Car's telemetry
     */
    public static final Telemetry telemetry = new Telemetry();
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="JSON methods">
    public static void SendMessage(PrintWriter writer, SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    public static void SendMessageAdvanced(PrintWriter writer, SendMsgAdvanced msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
//</editor-fold>
}
