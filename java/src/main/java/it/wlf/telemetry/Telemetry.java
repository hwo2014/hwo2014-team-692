package it.wlf.telemetry;

import it.wlf.core.components.Piece;
import it.wlf.core.json.recive.carpositions.Data;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ivan
 */
public class Telemetry {
 
    public List<TelemetryData> carTelemetry = new ArrayList<>();
    
    public void Register(long gameTick, double carSpeed, double throttle, Data data){
        carTelemetry.add(new TelemetryData(gameTick, carSpeed, throttle, data));
//        System.out.println("--TELEMETRY--");
//        System.out.println("tick: " + gameTick);
//        System.out.println("piece: " + data.piecePosition.pieceIndex);
//        System.out.println("piece distance: " + data.piecePosition.inPieceDistance);
//        System.out.println("speed: " + carSpeed);
//        System.out.println("throttle: " + throttle);
//        System.out.println("drift: " + data.angle);
//        System.out.println("-------------");
    }
}
