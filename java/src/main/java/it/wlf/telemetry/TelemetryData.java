package it.wlf.telemetry;

import it.wlf.core.json.recive.carpositions.Data;

/**
 *
 * @author Ivan
 */
public class TelemetryData {

    public long TimeTick;
    public double Throttle;
    public double Speed;
    public Data Data;
    
    public TelemetryData(long gameTick, double speed, double throttle, Data data){
        TimeTick = gameTick;
        Throttle = throttle;
        Speed = speed;
        Data = data;
    }
}
