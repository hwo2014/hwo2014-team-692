package it.wlf.enuemerator;

/**
 * Enumerator for type request
 *
 * @author Melknix
 */
public enum JsonType {

    /**
     * BOT: join the server
     */
    JOIN,
    /**
     * SERVER: return the color o car (KEY)
     */
    YOURCAR,
    /**
     * SERVER: start race, contains all track?
     */
    GAMEINIT,
    /**
     * SERVER: start the race
     */
    GAMESTART,
    /**
     * SERVER: each car position
     */
    CARPOSITIONS,
    /**
     * BOT: pedal to the metal!
     */
    THROTTLE,
    /**
     * SERVER: rece over, contains race result
     */
    GAMEEND,
    /**
     * SERVER: end of tournament
     */
    TOURNAMENTEND,
    /**
     * SERVER: car out of track
     */
    CRASH,
    /**
     * SERVER: car respown. Throttle will be set to zero.
     */
    SPAWN,
    /**
     * SERVER: car finish a lap
     */
    LAPFINISHED,
    /**
     * SERVER: car disqualified
     */
    DNF,
    /**
     * SERVER: finish the race
     */
    FINISH,
    /**
     * BOT: car switch the lane
     */
    SWITCHLANE,
    /**
     * TEST: create race
     */
    CREATERACE,
    /**
     * TEST: join to a race
     */
    JOINRACE,
    /**
     * BOT: ping
     */
    PING,
    /**
     * SERVER: Errore
     */
    ERROR,
    /**
     * Turbo message
     */
    TURBOAVAILABLE;

    @Override
    public String toString() {
        switch (this) {
            case JOIN:
                return "join";
            case YOURCAR:
                return "yourCar";
            case GAMEINIT:
                return "gameInit";
            case GAMESTART:
                return "gameStart";
            case CARPOSITIONS:
                return "carPositions";
            case THROTTLE:
                return "throttle";
            case GAMEEND:
                return "gameEnd";
            case TOURNAMENTEND:
                return "tournamentEnd";
            case CRASH:
                return "crash";
            case SPAWN:
                return "spawn";
            case LAPFINISHED:
                return "lapFinished";
            case DNF:
                return "dnf";
            case FINISH:
                return "finish";
            case SWITCHLANE:
                return "switchLane";
            case CREATERACE:
                return "createRace";
            case JOINRACE:
                return "joinRace";
            case PING:
                return "ping";
            case ERROR:
                return "error";
            case TURBOAVAILABLE:
                return "turboAvailable";
        }
        return "";
    }
}
