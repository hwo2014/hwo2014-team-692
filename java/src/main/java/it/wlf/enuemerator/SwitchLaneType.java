package it.wlf.enuemerator;

/**
 * Enum used for switch lane
 *
 * @author Melknix
 */
public enum SwitchLaneType {

    LEFT,
    RIGHT,
    NONE;

    @Override
    public String toString() {
        switch (this) {
            case LEFT:
                return "Left";
            case RIGHT:
                return "Right";
        }
        return "";
    }
}
