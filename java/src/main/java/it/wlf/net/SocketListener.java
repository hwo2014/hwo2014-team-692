package it.wlf.net;

import it.wlf.core.calc.CarSpeedCalculator;
import it.wlf.core.calc.Physics;
import it.wlf.core.calc.TrackAnalyser;
import it.wlf.core.json.MsgWrapper;
import it.wlf.core.json.common.testrace.BotId;
import it.wlf.core.json.recive.carpositions.CarPositions;
import it.wlf.core.json.recive.carpositions.Data;
import it.wlf.core.json.recive.crash.Crash;
import it.wlf.core.json.recive.dnf.Dnf;
import it.wlf.core.json.recive.finish.Finish;
import it.wlf.core.json.recive.gameend.GameEnd;
import it.wlf.core.json.recive.gameinit.GameInit;
import it.wlf.core.json.recive.lapfinished.LapFinished;
import it.wlf.core.json.recive.spawn.Spawn;
import it.wlf.core.json.recive.turboavailable.TurboAvailable;
import it.wlf.core.json.recive.yourcar.YourCar;
import it.wlf.core.json.send.join.Join;
import it.wlf.core.json.send.ping.Ping;
import it.wlf.core.json.send.testrace.createrace.CreateRace;
import it.wlf.core.json.send.testrace.joinrace.JoinRace;
import it.wlf.enuemerator.JsonType;
import it.wlf.utils.WLFStatic;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Socket Listener
 *
 * @author Melknix
 */
public class SocketListener extends Thread {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    private String host;
    private int port;
    private String botName = "";
    private String botKey = "";

    //TestRace
    private String password = "";
    private String trackName = "";
    private int carCount;
    private boolean start = true;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructors">
    public SocketListener(String host, int port, String botName, String botKey) {
        this.host = host;
        this.port = port;
        this.botName = botName;
        this.botKey = botKey;
    }

    public SocketListener(String host, int port, String botName, String botKey, String password, String trackName, int carCount, boolean start) {
        this.host = host;
        this.port = port;
        this.botName = botName;
        this.botKey = botKey;
        this.password = password;
        this.trackName = trackName;
        this.carCount = carCount;
        this.start = start;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public methods">
    @Override
    public void run() {
        String line = "";
        JsonType jsonType = JsonType.PING;
        String logSeparator = "==================================== ";
        try {

            Socket soc = new Socket(host, port);
            BufferedReader reader = new BufferedReader(new InputStreamReader(soc.getInputStream(), "utf-8"));
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(soc.getOutputStream(), "utf-8"));

            System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
            if (isTestRace()) {
                if (start) {
                    WLFStatic.SendMessage(writer, new CreateRace(password, carCount, new BotId(botName, botKey), trackName));
                    //Disabilita il create race
                    start = false;
                } else {
                    WLFStatic.SendMessage(writer, new JoinRace(new BotId(botName, botKey), trackName, password, carCount));
                }
            } else {
                WLFStatic.SendMessage(writer, new Join(botName, botKey));
            }

            boolean crashed = false;
            while (!soc.isClosed()) {
                line = reader.readLine();
                if (line == null) {
                    WLFStatic.SendMessage(writer, new Ping());
                    continue;
                }
                final MsgWrapper msgFromServer = WLFStatic.gson.fromJson(line, MsgWrapper.class);

                try {
                    jsonType = JsonType.valueOf(msgFromServer.msgType.toUpperCase());
                } catch (Exception e) {
                    jsonType = JsonType.PING;
                    System.out.println("JSON type unknow - " + msgFromServer.msgType);
                    System.out.println("JSON type unknow - " + line);
                }

//                System.out.println(line);
                switch (jsonType) {
                    case JOIN:
                        System.out.println("Joined");
                        break;
                    case YOURCAR:
                        WLFStatic.myCar.addDataFromJson(WLFStatic.gson.fromJson(msgFromServer.data.toString(), YourCar.class));
                        break;
                    case GAMEINIT:
                        System.out.println("Race init");
                        if (WLFStatic.track.originalPieces.isEmpty()) {
                            System.out.println("Analyze track");
                            //Tournament send 2 gameinit
                            GameInit gi = WLFStatic.gson.fromJson(msgFromServer.data.toString(), GameInit.class);
                            Thread trackAnalyser = new TrackAnalyser(gi);
                            trackAnalyser.start();
                        }
                        break;
                    case GAMESTART:
                        System.out.println("Race start");
                        break;
                    case CARPOSITIONS:
                        CarPositions cp = WLFStatic.gson.fromJson(line, CarPositions.class);
                        //TODO da spostare in un thread separato
                        Data myData = null;
                        for (Data data : cp.data) {
                            if (data.id.color.equalsIgnoreCase(WLFStatic.myCar.color)) {
                                myData = data;
                            }
                        }
                        Thread t = new CarSpeedCalculator(writer, myData, cp.gameTick, crashed);
                        t.start();
//                        System.out.println("Car speed: " + WLFStatic.myCar.currentSpeed + " distanza/tick");
                        break;
                    case TURBOAVAILABLE:
                        WLFStatic.turboEngine.turboData = WLFStatic.gson.fromJson(msgFromServer.data.toString(), TurboAvailable.class);
                        WLFStatic.turboEngine.turboAvaible = true;
                        WLFStatic.turboEngine.checkTurboNextTime = true;
                        System.out.println("Turbo avaible");
                        break;
                    case LAPFINISHED:
                        System.out.println("lapFinished");
                        LapFinished lf = WLFStatic.gson.fromJson(msgFromServer.data.toString(), LapFinished.class);
                        Date date = new Date(lf.lapTime.millis);
                        DateFormat formatter = new SimpleDateFormat("m:s:S");
                        System.out.println(logSeparator + formatter.format(date));
                        break;
                    case CRASH:
                        crashed = true;
                        System.out.println("crash");
                        Crash c = WLFStatic.gson.fromJson(msgFromServer.data.toString(), Crash.class);
                        WLFStatic.physics.OnCrash(c);
                        break;
                    case SPAWN:
                        crashed = false;
                        System.out.println("spawn");
                        Spawn s = WLFStatic.gson.fromJson(msgFromServer.data.toString(), Spawn.class);
                        break;
                    case DNF:
                        System.out.println("dnf");
                        Dnf d = WLFStatic.gson.fromJson(msgFromServer.data.toString(), Dnf.class);
                        break;
                    case FINISH:
                        System.out.println("finish");
                        Finish f = WLFStatic.gson.fromJson(msgFromServer.data.toString(), Finish.class);
                        break;
                    case GAMEEND:
                        System.out.println("Race end");
                        GameEnd ge = WLFStatic.gson.fromJson(msgFromServer.data.toString(), GameEnd.class);
                        if (WLFStatic.isQuickRace) {
                            soc.close();
                        }
                        break;
                    case TOURNAMENTEND:
                        System.out.println("Tournament end");
                        soc.close();
                        break;
                    case CREATERACE:
                        for (int i = 0; i < carCount - 1; i++) {
                            try {
                                Thread.sleep(1000l);
                            } catch (InterruptedException ex) {
                                System.out.println(ex.getMessage());
                            }
                            Thread bot = new SocketListener(host, port, "BOT" + i, botKey, password, trackName, carCount, false);
                            bot.start();
                        }
                        break;
                    case ERROR:
                        System.out.println(msgFromServer.data.toString());
                        break;
                    default:
                        System.out.println("Ping");
                        WLFStatic.SendMessage(writer, new Ping());
                }
            }
            System.out.println("Exit procedure");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println(line);
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Private Methods">
    private boolean isTestRace() {
        return !trackName.equals("");
    }
//</editor-fold>
}
