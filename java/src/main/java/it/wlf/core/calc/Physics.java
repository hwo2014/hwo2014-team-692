package it.wlf.core.calc;

import it.wlf.core.components.Piece;
import it.wlf.core.json.recive.carpositions.CarPositions;
import it.wlf.core.json.recive.carpositions.Data;
import it.wlf.core.json.recive.carpositions.PiecePosition;
import it.wlf.core.json.recive.crash.Crash;
import it.wlf.core.json.send.throttle.Throttle;
import it.wlf.utils.WLFStatic;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Ivan
 */
public class Physics {

    public double limitDriftAngle = 50;
    public double decelerationFactor = 2; //diminuzione % velocit‡ per per unit‡ di distanza 
    public double accelerationFactor = 2;
    public boolean needsToComputeAcceleration = true;

    private Data lastData;

    private double gameTick0 = -1;
    private double gameTick1 = -1;
    private double gameTick2 = -1;
    private double speed0 = -1;
    private double speed1 = -1;
    private double speed2 = -1;
    int step = 0;

    public void AnalyzeData(Data data) {        
        lastData = data;
    }

    public double ComputeAcceleration(long actualGameTick) {

        if (WLFStatic.myCar.currentSpeed < WLFStatic.myCar.MINIMUM_SPEED) {
            return 1;
        }

        if (step == 0) {
            step++;
            gameTick0 = actualGameTick;
            speed0 = WLFStatic.myCar.currentSpeed;
            return 1;
        } else if (step > 0 && step < 20) {
            step++;
            return 1;
        } else if (step == 20) {
            step++;
            gameTick1 = actualGameTick;
            speed1 = WLFStatic.myCar.currentSpeed;
            return 0;
        } else if (step > 20 && step < 40) {
            step++;
            return 0;
        } else if (step == 40) {
            step++;
            gameTick2 = actualGameTick;
            speed2 = WLFStatic.myCar.currentSpeed;
            return 0;
        } else {
            step++;
            accelerationFactor = (speed1 - speed0) / (gameTick1 - gameTick0);
            decelerationFactor = Math.abs((speed2 - speed1) / (gameTick2 - gameTick1));

            needsToComputeAcceleration = false;
            //System.out.println("acceleration " + accelerationFactor);
            //System.out.println("deceleration " + decelerationFactor);

            //a=Dv/Dt
            return 1;
        }
    }

    public void OnCrash(Crash crashData) {
        limitDriftAngle = Math.ceil(Math.abs(lastData.angle));
    }

    public boolean IsCloseToCrash(double angle) {
        //teniamoci ad una % del limite massimo
        //==> il sistema non Ë abbastanza reattivo per stare sotto a questo valore
        double percentage = 25;
        double maxAngle = WLFStatic.physics.limitDriftAngle - ((WLFStatic.physics.limitDriftAngle * percentage) / 100);

        return Math.abs(angle) >= maxAngle;
    }

    public boolean ShouldSlowDown(double angle) {
        //teniamoci ad una % del limite massimo
        //==> il sistema non Ë abbastanza reattivo per stare sotto a questo valore
        double percentage = 40;
        double maxAngle = WLFStatic.physics.limitDriftAngle - ((WLFStatic.physics.limitDriftAngle * percentage) / 100);

        return Math.abs(angle) >= maxAngle;
    }

    public boolean CanSpeedUp(double angle) {
        //teniamoci ad una % del limite massimo
        //==> il sistema non Ë abbastanza reattivo per stare sotto a questo valore
        double percentage = 40;
        double maxAngle = WLFStatic.physics.limitDriftAngle - ((WLFStatic.physics.limitDriftAngle * percentage) / 100);

        return Math.abs(angle) < maxAngle;
    }

    public double GetTeoricalSpeed(Piece piece, int index) {
        //piece.angle
        //w = v/r
        if (piece.angle == 0) {
            double distanceBeforeTurn = WLFStatic.track.GetSpaceBeforeTurn(index);
            if (distanceBeforeTurn > 150) {
                return 1;
            } else {
                return 1 - (Math.log10(distanceBeforeTurn) * decelerationFactor);
            }
        } else {
            double rad = piece.length / piece.radius;
            return 0.5;

            //approciando in curva rallenta
            //in mezzo alla curva mantieni
            //in uscita aumenta
        }
    }
}
