package it.wlf.core.calc;

import it.wlf.core.components.Piece;
import it.wlf.core.json.recive.carpositions.Data;
import it.wlf.core.json.send.switchlane.SwitchLane;
import it.wlf.core.json.send.throttle.Throttle;
import it.wlf.core.json.send.turbo.Turbo;
import it.wlf.enuemerator.SwitchLaneType;
import it.wlf.utils.WLFStatic;
import java.io.PrintWriter;

/**
 * Process the request
 *
 * @author Melknix
 */
public class CarSpeedCalculator extends Thread {
//<editor-fold defaultstate="collapsed" desc="Attributes">

    //private int ANGLELIMIT = 50;
    private PrintWriter writer;
    private long gameTick;
    private Data myData;
    private boolean crashed;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Contructor">
    public CarSpeedCalculator(PrintWriter writer, Data myData, long gameTick, boolean crashed) {
        this.writer = writer;
        this.myData = myData;
        this.gameTick = gameTick;
        this.crashed = crashed;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public methods">
    @Override
    public void run() {
        double speed = 0.0;
        boolean otherCommand = false;

        WLFStatic.myCar.ActualSpeed(myData, gameTick);
        WLFStatic.physics.AnalyzeData(myData);

        if (!crashed) {
            speed = WLFStatic.myPilot.GetThrottle(myData, gameTick);
        } else {
            WLFStatic.myPilot.Crashed(myData);
            speed = 1;
        }

        try {
            //cambio corsia
            if (WLFStatic.myCar.currentPicePosition != myData.piecePosition.pieceIndex) {
                WLFStatic.myCar.currentPicePosition = myData.piecePosition.pieceIndex;
                if (WLFStatic.track.pieces.get(myData.piecePosition.pieceIndex + 1).switchProperty) {
                    WLFStatic.turboEngine.checkTurboNextTime = true;
                    if (WLFStatic.track.pieces.get(myData.piecePosition.pieceIndex + 1).switchLane != SwitchLaneType.NONE) {
                        otherCommand = true;
                        WLFStatic.SendMessageAdvanced(writer, new SwitchLane(WLFStatic.track.pieces.get(myData.piecePosition.pieceIndex + 1).switchLane, gameTick));
//                        System.out.println("Switch send on pice " + myData.piecePosition.pieceIndex + " send " + WLFStatic.track.pieces.get(myData.piecePosition.pieceIndex + 1).switchLane);
                    }
                } else {
                    if (checkTurboToSend()) {
                        otherCommand = true;
                        WLFStatic.turboEngine.turboAvaible = false;
                        WLFStatic.turboEngine.checkTurboNextTime = false;
                        WLFStatic.SendMessage(writer, new Turbo());
                    }
                }
            } else {
                //speed = WLFStatic.myPilot.GetThrottle(myData, gameTick);
                //speed = WLFStatic.track.pieces.get(myData.piecePosition.pieceIndex).getSpeed(myData);

                if (WLFStatic.turboEngine.checkTurboNextTime) {
                    WLFStatic.turboEngine.checkTurboNextTime = false;
                    if (checkTurboToSend()) {
                        otherCommand = true;
                        WLFStatic.turboEngine.turboAvaible = false;
                        WLFStatic.SendMessage(writer, new Turbo());
                    }
                } else {
                    speed = WLFStatic.track.pieces.get(myData.piecePosition.pieceIndex).getSpeed(myData);
                    //speed = WLFStatic.myPilot.GetThrottle(myData, gameTick);
                }
            }
        } catch (Exception e) {
            System.out.println("Track Analyze not finish in piece " + myData.piecePosition.pieceIndex);
            System.out.println("Recalc");
            //speed = WLFStatic.physics.getTeoricalSpeed(WLFStatic.track.pieces.get(myData.piecePosition.pieceIndex), myData.piecePosition.pieceIndex);
            speed = WLFStatic.myPilot.GetThrottle(myData, gameTick);
        }

        if (WLFStatic.physics.needsToComputeAcceleration) {
            speed = WLFStatic.physics.ComputeAcceleration(gameTick);
        }

        if (!otherCommand) {
            //TODO angoli fittizzi, da calcolare crash oltre i 60 NON NECESSARIO CON CALCOLI CURVA CORRETTI
            //if (myData.angle > ANGLELIMIT || myData.angle < -ANGLELIMIT) {
            //TODO riduzione fittizzia, da calcolare
            WLFStatic.SendMessageAdvanced(writer, new Throttle(speed, gameTick));
//            System.out.println(speed);
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Private methods">
    private boolean checkTurboToSend() {
        if (!WLFStatic.turboEngine.turboAvaible) {
            return false;
        }
//        double futureSpeed = WLFStatic.turboEngine.turboData.turboDurationTicks / WLFStatic.myCar.currentSpeed;
//        double boostedLinearAcceleration = ((WLFStatic.myCar.currentSpeed - futureSpeed) / (0 - WLFStatic.turboEngine.turboData.turboDurationTicks)) * WLFStatic.turboEngine.turboData.turboFactor;
//        double distance = (WLFStatic.myCar.currentSpeed*WLFStatic.turboEngine.turboData.turboDurationTicks)+(1/2*boostedLinearAcceleration*(WLFStatic.turboEngine.turboData.turboDurationTicks^2));
        //TODO rivedere il calcolo del turbo NON FUNZIONA
        double distance = WLFStatic.myCar.currentSpeed * WLFStatic.turboEngine.turboData.turboFactor * WLFStatic.turboEngine.turboData.turboDurationTicks;
        int index = new Integer(myData.piecePosition.pieceIndex).intValue();
        double avaibleDistance = 0;
        try {
            Piece currentPiece = WLFStatic.track.pieces.get(index);
            while (currentPiece.angle == 0) {
//                System.out.println("Curernt angle" + currentPiece.angle);
                avaibleDistance += currentPiece.length;
                index++;
                currentPiece = WLFStatic.track.pieces.get(index);
            }
//            System.out.println("distance " + avaibleDistance +">="+ distance);
            return avaibleDistance >= distance && distance > 0;
        } catch (Exception e) {
            System.out.println("Track not analysed yet");
        }
        return false;
    }
//</editor-fold>

}
