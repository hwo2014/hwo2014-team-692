package it.wlf.core.calc;

import it.wlf.core.json.recive.gameinit.Cars;
import it.wlf.utils.WLFStatic;
import java.util.ArrayList;

/**
 * Thread to set car information
 * @author Melknix
 */
public class SetCarInformation extends Thread{
    
//<editor-fold defaultstate="collapsed" desc=" Attributes">
    public final ArrayList<Cars> cars;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public SetCarInformation(ArrayList<Cars> cars) {
        this.cars = cars;
    }    
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public methods">
    @Override
    public void run() {
        for (Cars c: cars) {
            if (c.id.color.equalsIgnoreCase(WLFStatic.myCar.color)){
                WLFStatic.myCar.lenght = c.dimensions.length;
                WLFStatic.myCar.width = c.dimensions.width;
                WLFStatic.myCar.guideFlagPosition = c.dimensions.guideFlagPosition;
                WLFStatic.myCar.mass = WLFStatic.myCar.lenght * WLFStatic.myCar.width * WLFStatic.MASSMULTIPLIER;
                break;
            }
        }
    }
//</editor-fold>
}
