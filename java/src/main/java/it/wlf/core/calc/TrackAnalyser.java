package it.wlf.core.calc;

import it.wlf.core.components.Piece;
import it.wlf.core.json.recive.gameinit.GameInit;
import it.wlf.core.json.recive.gameinit.Pieces;
import it.wlf.enuemerator.SwitchLaneType;
import it.wlf.utils.WLFStatic;

/**
 * Analyze and set data of track
 *
 * @author Melknix
 */
public class TrackAnalyser extends Thread {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    private final static double LEFT = -1;
    private final static double DOUBLELEFT = -1.5;
    private final static double RIGHT = 1;
    private final static double DOUBRIGHT = 1.5;
    private GameInit gameInit;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public TrackAnalyser(GameInit gameInit) {
        this.gameInit = gameInit;
    }

//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="Public methods">
    @Override
    public void run() {
        WLFStatic.track.lanes.addAll(gameInit.race.track.lanes);
        WLFStatic.track.originalPieces.addAll(gameInit.race.track.pieces); 
        WLFStatic.isQuickRace = gameInit.race.raceSession.durationMs == 0;
        
        Thread t = new SetCarInformation(gameInit.race.cars);
        t.start();

        double changeLanes = 0;
        int lastTurnPieceIndex = 0;
        int currentIndex = 0;

        for (Pieces p : gameInit.race.track.pieces) {
            Piece pice = new Piece(p.length, p.angle, p.switchProperty, p.radius);
            WLFStatic.track.pieces.add(pice);
            pice.speed = WLFStatic.physics.GetTeoricalSpeed(pice, currentIndex);
            if (p.angle != 0) {
                //TODO: lanciare la procedura dei conti                
                boolean turnbefore = false;
                if (currentIndex > 0) {
                    turnbefore = WLFStatic.track.pieces.get(currentIndex - 1).angle != 0;
                    WLFStatic.track.pieces.get(currentIndex - 1).nextTurn = true;
                }
                if (p.angle > 0) {
                    if (turnbefore) {
                        changeLanes += DOUBRIGHT;
                    } else {
                        changeLanes += RIGHT;
                    }
                } else {
                    if (turnbefore) {
                        changeLanes += DOUBLELEFT;
                    } else {
                        changeLanes += LEFT;
                    }
                }
            }
            if (p.switchProperty) {
                if (lastTurnPieceIndex != 0) {
                    if (changeLanes > 0) {
                        WLFStatic.track.pieces.get(lastTurnPieceIndex).switchLane = SwitchLaneType.RIGHT;
                    } else {
                        WLFStatic.track.pieces.get(lastTurnPieceIndex).switchLane = SwitchLaneType.LEFT;
                    }
                    changeLanes = 0;
                }
                lastTurnPieceIndex = new Integer(currentIndex).intValue();
            }
            currentIndex++;
        }

        for (Piece p : WLFStatic.track.pieces) {
            int index = WLFStatic.track.pieces.indexOf(p);
            if (index == 0) {
                p.prevPiece = WLFStatic.track.pieces.get(WLFStatic.track.pieces.size() - 1);
            } else {
                p.prevPiece = WLFStatic.track.pieces.get(index - 1);
            }
            if (index == WLFStatic.track.pieces.size() - 1) {
                p.nextPiece = WLFStatic.track.pieces.get(0);
            } else {
                p.nextPiece = WLFStatic.track.pieces.get(index + 1);
            }
        }
    }
//</editor-fold>
}
