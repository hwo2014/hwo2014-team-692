package it.wlf.core.components;

import it.wlf.core.json.recive.carpositions.Data;
import it.wlf.core.json.recive.gameinit.Pieces;
import it.wlf.enuemerator.SwitchLaneType;
import it.wlf.utils.WLFStatic;

/**
 * Piece information
 *
 * @author Melknix
 */
public class Piece extends Pieces {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public SwitchLaneType switchLane = SwitchLaneType.NONE;
    public double speed = 1;
    public boolean nextTurn = false;

    public Data lastData = null;    
    public Piece nextPiece = null;
    public Piece prevPiece = null;
    public double lastThrottle = 1;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Piece(double lenght, double angle, boolean switchProperty, double radius) {
        super(lenght, angle, switchProperty, radius);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public methods">
    public double getSpeed(Data myData) {
//        if (this.angle != 0) {
//            double r = this.radius + WLFStatic.track.lanes.get(myData.piecePosition.lane.endLaneIndex).distanceFromCenter;
//            double linearSpeed = WLFStatic.myCar.currentSpeed / r;
//            double angularSpeed = (myData.angle - WLFStatic.myCar.lastData.angle) / (WLFStatic.myCar.gameTick - WLFStatic.myCar.lastGameTick);
//            double centripetalAcceleration = WLFStatic.myCar.mass * (WLFStatic.myCar.currentSpeed * WLFStatic.myCar.currentSpeed) / r;
//            System.out.println("=====================================================");
//            System.out.println("Street angle: " + angle);
//            System.out.println("Car angle: " + myData.angle);
//            System.out.println("Linear speed: " + linearSpeed);
//            System.out.println("Speed: " + WLFStatic.myCar.currentSpeed);
//            System.out.println("Angular speed: " + angularSpeed);
//            System.out.println("Centripetal acceleration: " + centripetalAcceleration);
//            System.out.println("=====================================================");
//            speed = 1 - (WLFStatic.myCar.currentSpeed / 10);
//            System.out.println("Sent speed: " + speed);
//            return speed;
//        }
        if (nextTurn && angle == 0) {
            //TODO calcolare la decelerazione corretta
            return 0.45;
        }
        return speed;
    }
//</editor-fold>
}
