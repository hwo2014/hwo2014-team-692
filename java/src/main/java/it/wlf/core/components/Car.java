package it.wlf.core.components;

import it.wlf.core.json.recive.carpositions.Data;
import it.wlf.core.json.recive.carpositions.PiecePosition;
import it.wlf.core.json.recive.yourcar.YourCar;
import it.wlf.utils.WLFStatic;
import java.io.PrintWriter;

/**
 *
 * @author Melknix
 */
public class Car {

    public final double MINIMUM_SPEED = 1.5;
    
//<editor-fold defaultstate="collapsed" desc="Attributes">
    public String name = "";
    public String color = "";
    public int currentLaneIndex = 0;
    public double lenght = 0;
    public double width = 0;
    public double mass = 0;
    public double guideFlagPosition = 0;
    public double currentPicePosition = 0;
    public Data lastData = null;
    public PiecePosition piecePosition = null;
    public long lastGameTick = 0;
    public double currentSpeed = 0;
    public Data currentData = null;
    public long gameTick = 0;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public methods">
    public void addDataFromJson(YourCar json) {
        name = json.name;
        color = json.color;
    }

    public void ActualSpeed(Data actualPosition, long actualGameTick) {
        double result;
        if (currentData == null) {
            result = 0;
        } else {
            Double distance;
            if (currentData.piecePosition.pieceIndex != actualPosition.piecePosition.pieceIndex) {
                distance = WLFStatic.track.originalPieces.get(currentData.piecePosition.pieceIndex).length - currentData.piecePosition.inPieceDistance + actualPosition.piecePosition.inPieceDistance;
            } else {
                distance = actualPosition.piecePosition.inPieceDistance - currentData.piecePosition.inPieceDistance;
            }
            result = distance / (actualGameTick - gameTick);
        }
        lastData = currentData;
        currentData = actualPosition;
        lastGameTick = gameTick;
        gameTick = actualGameTick;
        currentSpeed = result;
    }
//</editor-fold>

}
