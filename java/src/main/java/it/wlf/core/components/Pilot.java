/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.wlf.core.components;

import it.wlf.core.json.recive.carpositions.Data;
import it.wlf.utils.WLFStatic;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Ivan
 */
public class Pilot {

    public double currentThrottle = 1;

    public double GetThrottle(Data currentData, long gameTick) {
        Piece currentPiece = WLFStatic.track.pieces.get(currentData.piecePosition.pieceIndex);
        Piece nextTurn = WLFStatic.track.GetNextCurve(currentData);

        if (WLFStatic.myCar.currentSpeed < WLFStatic.myCar.MINIMUM_SPEED) {
            currentThrottle = 1;
            currentPiece.lastData = currentData;
            currentPiece.lastThrottle = currentThrottle;
            WLFStatic.telemetry.Register(gameTick, WLFStatic.myCar.currentSpeed, currentThrottle, currentData);
            return currentThrottle;
        }

        if (nextTurn.angle > 44) {
            //v=s/t => t=s/v => s= t*v
            double breakspace = ((WLFStatic.myCar.currentSpeed - 3) / WLFStatic.physics.decelerationFactor) * WLFStatic.myCar.currentSpeed; //spazio necessario alla frenata
            if (breakspace < 0) {
                breakspace = 0;
            }

            double spaceBeforeTurn = WLFStatic.track.GetSpaceBeforeTurn(currentData);
            //System.out.println("breakspace: " + breakspace + " spaceBeforeTurn: " + spaceBeforeTurn);

            if (spaceBeforeTurn == 0) {
                double spaceBeforeStraight = WLFStatic.track.GetSpaceBeforeStraight(currentData);
                if (spaceBeforeStraight < currentPiece.length && !WLFStatic.physics.ShouldSlowDown(currentPiece.lastData.angle)) {
                    //currentThrottle += 0.1;
                } else {
                    currentThrottle -= 0.2; //TODO calcolare con fattore accelerazione
                }
            } else {
                if (breakspace <= spaceBeforeTurn) {
                    currentThrottle = 0;
                } else if (breakspace > spaceBeforeTurn + ((spaceBeforeTurn * 25) / 100)) {
                    currentThrottle = 0.2;
                } else {
                    currentThrottle += 0.2;
                }
            }
        } else {
            //controlla se l'ultima volta che si Ë passati su questo pezzo la velocit‡ Ë stata eccessiva
            if (currentPiece.angle != 0) {
                if (currentPiece.lastData != null) { //ricalcolo in base al giro precedente
                    if (WLFStatic.physics.ShouldSlowDown(currentPiece.lastData.angle)) {
                        currentThrottle = currentPiece.lastThrottle - 0.15;
                        if (WLFStatic.physics.IsCloseToCrash(currentPiece.lastData.angle)) {
                            currentThrottle = 0;
                        }
                    } else {
                        if (WLFStatic.physics.CanSpeedUp(currentPiece.lastData.angle)) {
                            currentThrottle += 0.25;
                        }
                    }
                }                           
            }
        }

        currentThrottle = fixThrottle(currentThrottle);
        currentPiece.lastData = currentData;
        currentPiece.lastThrottle = currentThrottle;
        WLFStatic.telemetry.Register(gameTick, WLFStatic.myCar.currentSpeed, currentThrottle, currentData);
        return currentThrottle;
    }

    public void Crashed(Data currentData) {
        Piece currentPiece = WLFStatic.track.pieces.get(currentData.piecePosition.pieceIndex);
        currentPiece.lastThrottle -= 0.3;
        currentPiece.prevPiece.lastThrottle -= 0.5;
    }

    private double fixThrottle(double value) {
        //conclusione
        if (value > 1) {
            return 1;
        } else if (value < 0) {
            return 0;
        }
        return value;
    }
}
