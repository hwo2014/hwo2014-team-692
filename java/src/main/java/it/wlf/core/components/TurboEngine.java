
package it.wlf.core.components;

import it.wlf.core.json.recive.turboavailable.TurboAvailable;

/**
 * TurboEngine object
 * @author Melknix
 */
public class TurboEngine {
//<editor-fold defaultstate="collapsed" desc="Attributes">
    public boolean turboAvaible = false;
    public TurboAvailable turboData = null;
    /**
     * if turbo avaible with switch piece, remember calc for next time
     */
    public boolean checkTurboNextTime = false;
//</editor-fold>
    
}
