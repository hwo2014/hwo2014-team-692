package it.wlf.core.components;

import it.wlf.core.json.recive.carpositions.Data;
import it.wlf.core.json.recive.gameinit.Lanes;
import it.wlf.core.json.recive.gameinit.Pieces;
import java.util.ArrayList;
import java.util.List;

/**
 * Trtack information
 *
 * @author Melknix
 */
public class Track {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public ArrayList<Lanes> lanes;
    public ArrayList<Piece> pieces;
    public ArrayList<Pieces> originalPieces;
    public int leftCount = 0;
    public int rightCount = 0;

//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Track() {
        lanes = new ArrayList<Lanes>();
        pieces = new ArrayList<Piece>();
        originalPieces = new ArrayList<Pieces>();
    }
//</editor-fold>

    public double GetSpaceBeforeTurn(Data piece) {
        //TODO: qua non funziona qualcosa... torna sempre ZERO
        int startIndex = piece.piecePosition.pieceIndex;
        double distance = 0;

        Piece p = pieces.get(startIndex);
        while (p.angle == 0) {
            distance += p.length;

            if (startIndex + 1 == pieces.size()) {
                startIndex = 0;
            } else {
                startIndex++;
            }
            p = pieces.get(startIndex);
        }

        distance -= piece.piecePosition.inPieceDistance;
        return distance > 0 ? distance : 0;
    }

    public double GetSpaceBeforeTurn(int pieceIndex) {
        double distance = 0;

        Pieces p = originalPieces.get(pieceIndex);
        while (p.angle == 0) {
            distance += p.length;

            if (pieceIndex + 1 == originalPieces.size()) {
                pieceIndex = 0;
            } else {
                pieceIndex++;
            }
            p = originalPieces.get(pieceIndex);
        }

        return distance > 0 ? distance : 0;
    }

    public Piece GetNextCurve(Data piece) {
        int startIndex = piece.piecePosition.pieceIndex;
        Piece p = pieces.get(startIndex);
        while (!p.nextTurn) {
            if (startIndex + 1 == originalPieces.size()) {
                startIndex = 0;
            } else {
                startIndex++;
            }
            p = pieces.get(startIndex);
        }
        return p;
    }

    public double GetSpaceBeforeStraight(Data piece) {
        int startIndex = piece.piecePosition.pieceIndex;
        double distance = 0;

        Piece p = pieces.get(startIndex);
        while (p.angle != 0) {
            distance += p.length;

            if (startIndex + 1 == pieces.size()) {
                startIndex = 0;
            } else {
                startIndex++;
            }
            p = pieces.get(startIndex);
        }

        return distance;
    }

    //calcola la curva totale ottenuta dalla somma delle curve consecutive con lo stesso raggio
    public Piece GetTotalCurve(Data piece) {
        int startIndex = piece.piecePosition.pieceIndex;
        Piece p = pieces.get(startIndex);
        double angle = p.angle;
        double radius = p.radius;
        double lenght = 0;
        while (p.angle == angle && p.radius == radius) {
            lenght += p.length;
            if (startIndex + 1 == originalPieces.size()) {
                startIndex = 0;
            } else {
                startIndex++;
            }
            p = pieces.get(startIndex);
        }
        return new Piece(lenght, angle, false, p.radius);
    }
}
