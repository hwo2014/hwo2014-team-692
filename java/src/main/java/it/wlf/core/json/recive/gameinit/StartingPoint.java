package it.wlf.core.json.recive.gameinit;

public class StartingPoint {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final double angle;
    public final Position position;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public StartingPoint(final double angle, final Position position) {
        this.angle = angle;
        this.position = position;
    }
//</editor-fold>
}
