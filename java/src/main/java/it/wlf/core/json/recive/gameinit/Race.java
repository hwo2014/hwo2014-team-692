package it.wlf.core.json.recive.gameinit;

import java.util.ArrayList;

public class Race {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final Track track;
    public final ArrayList<Cars> cars;
    public final RaceSession raceSession;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Race(final Track track, final ArrayList<Cars> cars, final RaceSession raceSession) {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }
//</editor-fold>
}
