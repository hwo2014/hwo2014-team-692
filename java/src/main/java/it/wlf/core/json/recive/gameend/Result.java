package it.wlf.core.json.recive.gameend;

public class Result {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final long ticks;
    public final long millis;
    public final int lap;
    public final int laps;
    public final String reason;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public Result(final long ticks, final long millis, final int lap, final int laps, final String reason) {
        this.ticks = ticks;
        this.millis = millis;
        this.lap = lap;
        this.laps = laps;
        this.reason = reason;
    }
//</editor-fold>

}
