package it.wlf.core.json.recive.spawn;

/**
 * JSON model for spawn
 *
 * @author Melknix
 */
public class Spawn {
    
//<editor-fold defaultstate="collapsed" desc="Attributes">

    public final String name;
    public final String color;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Spawn(final String name, final String color) {
        this.name = name;
        this.color = color;
    }
//</editor-fold>
}
