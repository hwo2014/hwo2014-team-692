package it.wlf.core.json.send.testrace.createrace;

import it.wlf.core.json.common.testrace.BotId;
import it.wlf.enuemerator.JsonType;
import it.wlf.core.json.SendMsg;

/**
 * JSON model to create race
 * @author Melknix
 */
public class CreateRace extends SendMsg{

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final String password;
    public final int carCount;
    public final BotId botId;
    public final String trackName;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public CreateRace(final String password, final int carCount, final BotId botId, final String trackName){
        this.password = password;
        this.carCount = carCount;
        this.botId = botId;
        this.trackName = trackName;
    }
//</editor-fold>

    @Override
    protected String msgType() {
        return JsonType.CREATERACE.toString();
    }
}
