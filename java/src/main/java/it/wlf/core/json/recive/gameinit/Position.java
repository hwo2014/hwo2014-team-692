package it.wlf.core.json.recive.gameinit;

public class Position {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final double x;
    public final double y;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Position(final double x, final double y) {
        this.x = x;
        this.y = y;
    }
//</editor-fold>
}
