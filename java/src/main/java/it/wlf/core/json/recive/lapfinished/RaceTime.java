package it.wlf.core.json.recive.lapfinished;

public class RaceTime {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final long ticks;
    public final long millis;
    public final int laps;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public RaceTime(final long ticks, final long millis, final int laps) {
        this.ticks = ticks;
        this.millis = millis;
        this.laps = laps;
    }
//</editor-fold>

}
