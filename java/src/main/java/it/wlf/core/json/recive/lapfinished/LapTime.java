package it.wlf.core.json.recive.lapfinished;

public class LapTime {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final int lap;
    public long ticks;
    public long millis;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public LapTime(final int lap, final long ticks, final long millis) {
        this.lap = lap;
        this.millis = millis;
        this.ticks = ticks;
    }
//</editor-fold>

}
