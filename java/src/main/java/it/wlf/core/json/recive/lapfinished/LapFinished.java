package it.wlf.core.json.recive.lapfinished;

import it.wlf.core.json.common.Car;

/**
 * JSON model for lapFinished
 *
 * @author Melknix
 */
public class LapFinished {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final RaceTime raceTime;
    public final Ranking ranking;
    public final Car car;
    public final LapTime lapTime;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public LapFinished(final RaceTime raceTime, final Ranking ranking, final Car car, final LapTime lapTime) {
        this.raceTime = raceTime;
        this.ranking = ranking;
        this.car = car;
        this.lapTime = lapTime;
    }
//</editor-fold>

}
