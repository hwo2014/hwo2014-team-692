package it.wlf.core.json.recive.lapfinished;

public class Ranking {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    private final int overall;
    private final int fastestLap;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Ranking(final int overall, final int fastestLap) {
        this.overall = overall;
        this.fastestLap = fastestLap;
    }
//</editor-fold>

}
