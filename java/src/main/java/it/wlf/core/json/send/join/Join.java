package it.wlf.core.json.send.join;

import it.wlf.core.json.SendMsg;

/**
 * Send Join message
 * @author Melknix
 */
public class Join extends SendMsg {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final String name;
    public final String key;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Contructor">
    public Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public methods">
    @Override
    public String msgType() {
        return "join";
    }
//</editor-fold>
}
