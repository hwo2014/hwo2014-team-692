package it.wlf.core.json.common;

public class Id {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final String name;
    public final String color;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Id(final String name, final String color) {
        this.name = name;
        this.color = color;
    }
//</editor-fold>

}
