package it.wlf.core.json.recive.dnf;

import it.wlf.core.json.common.Car;

/**
 * JSON model for dnf
 * @author Melknix
 */
public class Dnf {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final Car car;
    public final String reason;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Dnf(final Car car, final String reason){
        this.car = car;
        this.reason = reason;
    }
//</editor-fold>
}
