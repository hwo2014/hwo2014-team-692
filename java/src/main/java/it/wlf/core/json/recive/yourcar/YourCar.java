package it.wlf.core.json.recive.yourcar;

/**
 * Json map yourCar
 *
 * @author Melknix
 */
public class YourCar {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final String name;
    public final String color;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public YourCar(final String name, final String color) {
        this.name = name;
        this.color = color;
    }
//</editor-fold>
}
