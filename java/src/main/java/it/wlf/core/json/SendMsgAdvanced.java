package it.wlf.core.json;

import it.wlf.utils.WLFStatic;

/**
 * SendMsg advanced for gameTick
 * @author Melknix
 */
public abstract class SendMsgAdvanced{
    
//<editor-fold defaultstate="collapsed" desc="Public methods">
    public String toJson() {
        return WLFStatic.gson.toJson(new MsgWrapperAdvanced(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
    
    protected abstract long gameTick();
//</editor-fold>
    
}
