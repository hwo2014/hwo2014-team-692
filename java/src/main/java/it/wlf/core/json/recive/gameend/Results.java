package it.wlf.core.json.recive.gameend;

import it.wlf.core.components.Car;

public class Results {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final Car car;
    public final Result result;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Results(final Car car, final Result result){
        this.car = car;
        this.result = result;
    }
//</editor-fold>
}
