package it.wlf.core.json.send.ping;

import it.wlf.core.json.SendMsg;

/**
 * Send ping message
 *
 * @author Melknix
 */
public class Ping extends SendMsg {

//<editor-fold defaultstate="collapsed" desc="Public methods">
    @Override
    public String msgType() {
        return "ping";
    }
//</editor-fold>
}
