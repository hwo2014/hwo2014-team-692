package it.wlf.core.json.recive.gameinit;

import java.util.ArrayList;

public class Track {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final StartingPoint startingPoint;
    public final String id;
    public final ArrayList<Lanes> lanes;
    public final String name;
    public final ArrayList<Pieces> pieces;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Track(final StartingPoint startingPoint, final String id, final ArrayList<Lanes> lanes, final String name, final ArrayList<Pieces> pieces) {
        this.startingPoint = startingPoint;
        this.id = id;
        this.lanes = lanes;
        this.name = name;
        this.pieces = pieces;
    }
//</editor-fold>

}
