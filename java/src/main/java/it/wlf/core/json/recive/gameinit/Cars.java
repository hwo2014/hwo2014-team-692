package it.wlf.core.json.recive.gameinit;

import it.wlf.core.json.common.Id;

public class Cars {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final Id id;
    public final Dimensions dimensions;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Cars(final Id id, final Dimensions dimensions) {
        this.id = id;
        this.dimensions = dimensions;
    }
//</editor-fold>

}
