package it.wlf.core.json.recive.carpositions;

import it.wlf.core.json.common.Id;

/**
 * JSON model for carPositions
 *
 * @author Melknix
 */
public class Data {
//<editor-fold defaultstate="collapsed" desc="Attributes">

    public final Id id;
    public final double angle;
    public final PiecePosition piecePosition;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Data(Id id, final double angle, final PiecePosition piecePosition) {
        this.id = id;
        
        if (angle < 359 && angle > -359) {
            this.angle = angle;
        } else {
            this.angle = 0;
        }

        this.piecePosition = piecePosition;
    }
//</editor-fold>
}
