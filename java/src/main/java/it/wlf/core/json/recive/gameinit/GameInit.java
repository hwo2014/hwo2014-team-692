package it.wlf.core.json.recive.gameinit;

/**
 * JSON Parser for gameInit
 *
 * @author Melknix
 */
public class GameInit {
//<editor-fold defaultstate="collapsed" desc="Attributes">

    public final Race race;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public GameInit(final Race race) {
        this.race = race;
    }
//</editor-fold>
}
