
package it.wlf.core.json.recive.turboavailable;

/**
 * JSON model for turbo
 * @author Melknix
 */
public class TurboAvailable {
//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final double turboFactor;
    public final int turboDurationTicks;
    public final double turboDurationMilliseconds;
    
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public TurboAvailable(double turboFactor, int turboDurationTicks, double turboDurationMilliseconds) {
        this.turboFactor = turboFactor;
        this.turboDurationTicks = turboDurationTicks;
        this.turboDurationMilliseconds = turboDurationMilliseconds;
    }
//</editor-fold>
}
