package it.wlf.core.json.send.switchlane;

import it.wlf.core.json.SendMsgAdvanced;
import it.wlf.enuemerator.SwitchLaneType;

/**
 * Send JSON switchLane
 * @author Melknix
 */
public class SwitchLane extends SendMsgAdvanced {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final String switchLane;
    private long gameTick;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public SwitchLane(SwitchLaneType switchLaneType, long gameTick){
        this.switchLane = switchLaneType.toString();
        this.gameTick = gameTick;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public methods">
    @Override
    protected String msgType() {
        return "switchLane";
    }
    
    @Override
    protected Object msgData() {
        return switchLane;
    }
    
    @Override
    protected long gameTick() {
        return gameTick;
    }
//</editor-fold>
   
}
