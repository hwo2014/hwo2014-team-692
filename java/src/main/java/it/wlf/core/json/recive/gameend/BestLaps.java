package it.wlf.core.json.recive.gameend;

import it.wlf.core.json.common.Car;

public class BestLaps {
	
//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final Car car;
    public final Result result;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public BestLaps(final Car car, final Result result){
        this.car = car;
        this.result = result;
    }
//</editor-fold>
}
