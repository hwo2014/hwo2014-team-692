package it.wlf.core.json.recive.gameinit;

public class RaceSession {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final boolean quickRace;
    public final int laps;
    public final long maxLapTimeMs;
    public final long durationMs;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public RaceSession(final boolean quickRace, final int laps, final long maxLapTimeMs, final long durationMS) {
        this.quickRace = quickRace;
        this.laps = laps;
        this.maxLapTimeMs = maxLapTimeMs;
        this.durationMs = durationMS;
    }
//</editor-fold>
}
