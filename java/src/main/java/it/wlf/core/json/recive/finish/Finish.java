package it.wlf.core.json.recive.finish;

import it.wlf.core.json.common.Car;

/**
 * JSON model for finish
 * @author Melknix
 */
public class Finish {
    
//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final Car car;
    public final String reason;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Finish(final Car car, final String reason){
        this.car = car;
        this.reason = reason;
    }
//</editor-fold>
    
}
