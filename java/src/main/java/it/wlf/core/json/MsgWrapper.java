package it.wlf.core.json;

/**
 * Message wrapper for JSON
 *
 * @author Melknix
 */
public class MsgWrapper {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final String msgType;
    public final Object data;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Contructors">
    public MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
//</editor-fold>
}
