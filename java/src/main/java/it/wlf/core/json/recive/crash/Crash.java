
package it.wlf.core.json.recive.crash;

/**
 * JSON model for crash
 * @author Melknix
 */
public class Crash {
    
//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final String name;
    public final String color;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Crash(final String name, final String color){
        this.name = name;
        this.color = color;
    }
//</editor-fold>
    
}
