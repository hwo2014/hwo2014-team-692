package it.wlf.core.json.recive.carpositions;

import it.wlf.core.json.recive.carpositions.Lane;

public class PiecePosition {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final int lap;
    public final double inPieceDistance;
    public final Lane lane;
    public final int pieceIndex;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public PiecePosition(final int lap, final int inPieceDistance, final Lane lane, final int pieceIndex) {
        this.lap = lap;
        this.inPieceDistance = inPieceDistance;
        this.pieceIndex = pieceIndex;
        this.lane = lane;
    }
//</editor-fold>
}
