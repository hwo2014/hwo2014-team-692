package it.wlf.core.json;

import it.wlf.utils.WLFStatic;

/**
 * SendMessage
 * @author Melknix
 */
public abstract class SendMsg {

//<editor-fold defaultstate="collapsed" desc="Public methods">
    public String toJson() {
        return WLFStatic.gson.toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
//</editor-fold>
}
