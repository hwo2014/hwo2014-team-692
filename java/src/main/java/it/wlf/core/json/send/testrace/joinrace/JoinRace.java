package it.wlf.core.json.send.testrace.joinrace;

import it.wlf.enuemerator.JsonType;
import it.wlf.core.json.SendMsg;
import it.wlf.core.json.common.testrace.BotId;

/**
 * JSON model for join testrace
 * @author Melknix
 */
public class JoinRace extends SendMsg{
    
//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final BotId botId;
    public final String trackName;
    public final String password;
    public final int carCount;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public JoinRace(final BotId botId, final String trackName, final String password, final int carCount){
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public methods">
    @Override
    protected String msgType() {
        return JsonType.JOINRACE.toString();
    }
//</editor-fold>
    
}
