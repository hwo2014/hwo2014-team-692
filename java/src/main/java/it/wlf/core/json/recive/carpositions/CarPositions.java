package it.wlf.core.json.recive.carpositions;

import java.util.ArrayList;

/**
 * JSON model for carPositions
 *
 * @author Melknix
 */
public class CarPositions {
//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final ArrayList<Data> data;
    public final long gameTick;
    public final String gameId;
    public final  String msgType;
   
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public CarPositions(final ArrayList<Data> data, final long gameTick, final String gameId, final String msgType) {
        this.data = data;
        this.gameTick = gameTick;
        this.gameId = gameId;
        this.msgType = msgType;
    }
//</editor-fold>
}
