package it.wlf.core.json.recive.gameinit;

public class Dimensions {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final double width;
    public final double guideFlagPosition;
    public final double length;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Dimensions(final double width, final double guideFlagPosition, final double length){
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
        this.length = length;
    }
//</editor-fold>    
}
