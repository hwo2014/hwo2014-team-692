package it.wlf.core.json.recive.gameinit;

public class Lanes {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final int index;
    public final int distanceFromCenter;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Lanes(final int index, final int distanceFromCenter) {
        this.index = index;
        this.distanceFromCenter = distanceFromCenter;
    }
//</editor-fold>
}
