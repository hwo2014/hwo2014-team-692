package it.wlf.core.json.recive.gameend;

import java.util.ArrayList;

/**
 * JSON model game end
 * @author Melknix
 */
public class GameEnd {
    
//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final ArrayList<Results> results;
    public final ArrayList<BestLaps> bestLaps;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public GameEnd(final ArrayList<Results> results, final ArrayList<BestLaps> bestLaps){
        this.results = results;
        this.bestLaps = bestLaps;
    }
//</editor-fold>

}
