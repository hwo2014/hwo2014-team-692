package it.wlf.core.json.common.testrace;

public class BotId {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final String name;
    public final String key;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public BotId(final String name, final String key) {
        this.name = name;
        this.key = key;
    }
//</editor-fold>
}
