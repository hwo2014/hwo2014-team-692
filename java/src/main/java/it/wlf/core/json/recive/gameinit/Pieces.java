package it.wlf.core.json.recive.gameinit;

import com.google.gson.annotations.SerializedName;

public class Pieces {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final double length;
    public final double angle;
    @SerializedName("switch")
    public final boolean switchProperty;
    public final double radius;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructors">
    public Pieces(final double lenght, final double angle, final boolean switchProperty, final double radius) {
        this.length = lenght;
        this.angle = angle;
        this.radius = radius;
        this.switchProperty = switchProperty;
    }
//</editor-fold>

}
