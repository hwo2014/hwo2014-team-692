package it.wlf.core.json.send.throttle;

import it.wlf.core.json.SendMsgAdvanced;

/**
 * Json map throttle message
 * @author Melknix
 */
public class Throttle extends SendMsgAdvanced {
    
//<editor-fold defaultstate="collapsed" desc="Attributes">
    private double value;
    private long gameTick;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Contructor">
    public Throttle(double value, long gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public methods">
    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
    
    @Override
    protected long gameTick() {
        return gameTick;
    }
//</editor-fold>

}
