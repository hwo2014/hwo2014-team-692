package it.wlf.core.json.recive.carpositions;

public class Lane {

//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final int startLaneIndex;
    public final int endLaneIndex;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Lane(final int startLaneIndex, final int endLaneIndex) {
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }
//</editor-fold>
}
