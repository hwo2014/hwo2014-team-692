package it.wlf.core.json;

/**
 * Use to send Tick
 * @author Melknix
 */
public class MsgWrapperAdvanced extends MsgWrapper{
    
//<editor-fold defaultstate="collapsed" desc="Attributes">
    public final long gameTick;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public MsgWrapperAdvanced(final String msgType,final Object data,final long gameTick) {
        super(msgType, data);
        this.gameTick = gameTick;
    }
    
    public MsgWrapperAdvanced(final SendMsgAdvanced sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), sendMsg.gameTick());
    }
//</editor-fold>
    
}
