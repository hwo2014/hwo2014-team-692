
package it.wlf.core.json.send.turbo;

import it.wlf.core.json.SendMsg;

/**
 * JSON send message
 * @author Melknix
 */
public class Turbo extends SendMsg{
    
//<editor-fold defaultstate="collapsed" desc="Attributes">
    private final static String message = "Go, Go, Goooooooooooooooooooo!!!! WLF!!!";
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public Turbo(){
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public methods">
    @Override
    protected String msgType() {
        return "turbo";
    }
    
    @Override
    protected Object msgData() {
        return message;
    }
//</editor-fold>  
    
}
